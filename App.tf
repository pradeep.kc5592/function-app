
     "function key": {
        "type": "string",
       "value": "[listsecrets(resourceId('Microsoft.Web/sites/functions', parameters('Function-App-Name'), variables ('functionName')),'2015-08-01').key]"
    },


          "functionkeys": {
            "type": "object",                                                                               
            "value": "[listkeys(concat(variables('functionAppId'), '/host/default'), '2018-11-01')]"
    },


         "function Url": {
        "type": "string",
        "value": "[listsecrets(resourceId('Microsoft.Web/sites/functions', parameters('Function-App-Name'), variables ('functionName')),'2015-08-01').trigger_url]"
    } ,
